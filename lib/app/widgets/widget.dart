import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:get/get.dart';
import 'package:test/app/routes/app_pages.dart';
import 'package:test/app/style/app_color.dart';
import 'package:test/models/model.dart';
import 'package:multiavatar/multiavatar.dart';
part 'card_book.dart';
part 'avatar_image.dart';
