part of 'widget.dart';

class CardBook extends StatelessWidget {
  final Book book;
  final void Function() onTap;
  final void Function() onTapDelete;

  const CardBook(
      {super.key,
      required this.book,
      required this.onTap,
      required this.onTapDelete});

  @override
  Widget build(BuildContext context) {
    double _width = 80, _height = 110;
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(top: 15),
      child: Row(
        children: [
          Stack(children: [
            Container(
                padding: EdgeInsets.only(bottom: 50, right: 40),
                width: _width,
                height: _height,
                decoration: BoxDecoration(
                  color: AppColor.primaryColor,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(1, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Container(
                  width: _width / 2,
                  height: _height / 2,
                  decoration: BoxDecoration(
                      color:
                          Color(Random().nextInt(0xffffffff)).withAlpha(0xff),
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(15))),
                )),
            Container(
              width: _width,
              height: _height,
              padding: EdgeInsets.all(8),
              child: AvatarImage(
                book.cover ??
                    "https://penerbitdeepublish.com/wp-content/uploads/2020/11/Cover-Buku-DIGITAL-MARKETING-MELALUI-APLIKASI-PLAYSTORE_Usman-Chamdani-depan-scaled-1.jpg",
                radius: 8,
              ),
            )
          ]),
          SizedBox(
            width: 18,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 40,
                width: 240,
                child: Text(
                  book.title,
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              RichText(
                  text: TextSpan(children: [
                TextSpan(
                    text: book.price.toString(),
                    style: TextStyle(
                        fontSize: 16,
                        color: AppColor.primaryColor,
                        fontWeight: FontWeight.w500)),
                TextSpan(text: "   "),
              ]))
            ],
          )
        ],
      ),
    );
  }
}
