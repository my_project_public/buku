import 'package:get/get.dart';
import 'package:test/app/modules/AddBook/bindings/add_book_bindings.dart';
import 'package:test/app/modules/AddBook/views/add_book_view.dart';
import 'package:test/app/modules/DetailBook/bindings/detail_book_bindings.dart';
import 'package:test/app/modules/DetailBook/views/detail_book_view.dart';
import 'package:test/app/modules/Home/bindings/home_bindings.dart';
import 'package:test/app/modules/UpdateBook/bindings/update_book_bindings.dart';
import 'package:test/app/modules/UpdateBook/views/update_book_view.dart';

import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.ADDBOOK,
      page: () => AddBookView(),
      binding: AddBookBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.UPDATEBOOK,
      page: () => UpdateBookView(),
      binding: UpdateBookBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
    GetPage(
      name: _Paths.DETAILBOOK,
      page: () => DetailBookView(),
      binding: DetailBookBinding(),
      transition: Transition.noTransition,
      transitionDuration: Duration.zero,
    ),
  ];
}
