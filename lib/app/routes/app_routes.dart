part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const ADDBOOK = _Paths.ADDBOOK;
  static const UPDATEBOOK = _Paths.UPDATEBOOK;
  static const DETAILBOOK = _Paths.DETAILBOOK;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const ADDBOOK = '/add-book';
  static const UPDATEBOOK = '/update-book';
  static const DETAILBOOK = '/detail-book';
}
