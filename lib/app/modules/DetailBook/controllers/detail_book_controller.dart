import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test/services/services.dart';
import '../../../../models/model.dart';

class DetailBookController extends GetxController {
  final count = 0.obs;
  List argument = json.decode(Get.arguments);
  TextEditingController titleC = TextEditingController();
  TextEditingController isbnC = TextEditingController();
  TextEditingController descC = TextEditingController();
  TextEditingController dateC = TextEditingController();
  TextEditingController priceC = TextEditingController();
  String code = '';
  late Map<String, dynamic> book;

  RxString selectedValue = RxString("1");
  ApiReturnValue category =
      ApiReturnValue(value: null, message: "", statusCode: null);
  ApiReturnValue bookReturn =
      ApiReturnValue(value: null, message: "", statusCode: null);

  final List<String> items = [
    'Ibadah',
    'IT',
  ];

  @override
  void onInit() {
    List argument1 = json.decode(Get.arguments);

    if (Get.arguments != null) {
      if (argument1[1] != 1) {
        book = argument1[1];
        titleC.text = book['title'];
        isbnC.text = book['isbn'];
        descC.text = book['desc'];
        priceC.text = book['price'].toString();
        code = book['code'];
      }
    }
    super.onInit();
  }

  @override
  void onReady() {
    List argument1 = json.decode(Get.arguments);

    if (Get.arguments != null) {
      if (argument1[1] != 1) {
        book = argument1[1];
        titleC.text = book['title'];
        isbnC.text = book['isbn'];
        descC.text = book['desc'];
        priceC.text = book['price'].toString();
        code = book['code'];
      }
    }
    super.onReady();
  }

  @override
  void onClose() {
    titleC.text = '';
    isbnC.text = '';
    descC.text = '';
    priceC.text = '';
    selectedValue.value = '0';
    super.onClose();
  }

  void increment() => count.value++;

  RxBool isLoading = RxBool(false);
  void getCategory() async {
    try {
      isLoading.value = true;
      // ApiReturnValue category1 =
      //     await CategoryServices.getCategory() as ApiReturnValue;
      // category = category1;

      category.value = categoryDummy;
    } finally {
      isLoading.value = false;
    }
  }

  void deleteBook(String code1) async {
    try {
      isLoading.value = true;
      ApiReturnValue category1 =
          await BookServices.deleteBook(code1) as ApiReturnValue;
      bookReturn = category1;
    } finally {
      isLoading.value = false;
    }
  }
}
