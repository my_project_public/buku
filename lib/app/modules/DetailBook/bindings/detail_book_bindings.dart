import 'package:get/get.dart';
import 'package:test/app/modules/DetailBook/controllers/detail_book_controller.dart';

class DetailBookBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailBookController>(
      () => DetailBookController(),
    );
  }
}
