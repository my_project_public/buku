import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test/app/modules/Home/controllers/home_controller.dart';
import 'package:test/services/services.dart';
import '../../../../models/model.dart';

class AddBookController extends GetxController {
  final count = 0.obs;
  TextEditingController codeC = TextEditingController();
  TextEditingController titleC = TextEditingController();
  TextEditingController isbnC = TextEditingController();
  TextEditingController descC = TextEditingController();
  TextEditingController dateC = TextEditingController();
  TextEditingController priceC = TextEditingController();
  RxString selectedValue = RxString("1");
  ApiReturnValue category =
      ApiReturnValue(value: null, message: "", statusCode: null);
  ApiReturnValue book =
      ApiReturnValue(value: null, message: "", statusCode: null);

  final List<String> items = [
    'Ibadah',
    'IT',
  ];

  @override
  void onInit() {
    Get.find<HomeController>().onInit();
    getCategory();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;

  RxBool isLoading = RxBool(false);
  RxBool isLoading1 = RxBool(false);
  void getCategory() async {
    try {
      isLoading.value = true;
      // ApiReturnValue category1 =
      //     await CategoryServices.getCategory() as ApiReturnValue;
      // category = category1;

      category.value = categoryDummy;
    } finally {
      isLoading.value = false;
    }
  }

  void storeBook() async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 = await BookServices.storeBook(
          codeC.text,
          titleC.text,
          isbnC.text,
          descC.text,
          selectedValue.value,
          dateC.text,
          priceC.text) as ApiReturnValue;
      book = category1;
    } finally {
      isLoading1.value = false;
    }
  }
}
