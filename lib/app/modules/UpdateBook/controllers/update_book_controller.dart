import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test/services/services.dart';
import '../../../../models/model.dart';

class UpdateBookController extends GetxController {
  final count = 0.obs;

  late Map<String, dynamic> book;

  TextEditingController titleC = TextEditingController();
  TextEditingController isbnC = TextEditingController();
  TextEditingController descC = TextEditingController();
  TextEditingController dateC = TextEditingController();
  TextEditingController priceC = TextEditingController();
  TextEditingController codeC = TextEditingController();
  RxString selectedValue = RxString("1");
  ApiReturnValue category =
      ApiReturnValue(value: null, message: "", statusCode: null);
  ApiReturnValue uBook =
      ApiReturnValue(value: null, message: "", statusCode: null);

  final List<String> items = [
    'Ibadah',
    'IT',
  ];

  @override
  void onInit() {
    if (Get.arguments != null) {
      List argument1 = json.decode(Get.arguments);

      if (argument1[1] != 1) {
        book = argument1[1];
        titleC.text = book['title'];
        isbnC.text = book['isbn'];
        descC.text = book['desc'];
        priceC.text = book['price'].toString();
        codeC.text = book['code'].toString();
        selectedValue.value = book['categoryId'].toString();
      }
    }
    getCategory();

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    titleC.text = '';
    isbnC.text = '';
    descC.text = '';
    priceC.text = '';
    codeC.text = '';
    selectedValue.value = '0';
    super.onClose();
  }

  void increment() => count.value++;

  RxBool isLoading = RxBool(false);
  RxBool isLoading1 = RxBool(false);

  void getCategory() async {
    try {
      isLoading.value = true;
      // ApiReturnValue category1 =
      //     await CategoryServices.getCategory() as ApiReturnValue;
      // category = category1;

      category.value = categoryDummy;
    } finally {
      isLoading.value = false;
    }
  }

  void updateBook() async {
    try {
      isLoading1.value = true;
      ApiReturnValue category1 = await BookServices.updateBook(
          codeC.text,
          titleC.text,
          isbnC.text,
          descC.text,
          selectedValue.value,
          dateC.text,
          priceC.text) as ApiReturnValue;
      uBook = category1;
    } finally {
      isLoading1.value = false;
    }
  }
}
