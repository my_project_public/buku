import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:test/app/modules/DetailBook/controllers/detail_book_controller.dart';
import 'package:test/app/modules/Home/controllers/home_controller.dart';
import 'package:test/app/modules/Home/views/home_view.dart';
import 'package:test/app/modules/UpdateBook/controllers/update_book_controller.dart';
import 'package:test/app/routes/app_pages.dart';
import 'package:test/app/style/app_color.dart';
import 'package:test/app/widgets/custom_alert.dart';
import 'package:test/app/widgets/custom_input.dart';

class UpdateBookView extends GetView<UpdateBookController> {
  const UpdateBookView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Obx(
          () => Container(
            width: MediaQuery.of(context).size.width,
            height: 48,
            margin: const EdgeInsets.only(top: 8, bottom: 28),
            child: ElevatedButton(
              onPressed: () {
                controller.updateBook();
                Future.delayed(Duration(milliseconds: 3000), () {
                  if (controller.isLoading.value == false) {
                    if (controller.uBook.statusCode == 200) {
                      CustomAlert.show(
                        title: 'Buku Success',
                        description: "Buku berhasil diupdate!",
                        onTap: () {
                          Get.toNamed(Routes.HOME);
                        },
                      );
                      Future.delayed(Duration(milliseconds: 3000), () {
                        Get.delete<HomeController>();
                        Get.delete<DetailBookController>();
                        Get.delete<UpdateBookController>();

                        Get.toNamed(Routes.HOME);
                      });
                    } else {
                      CustomAlert.show(
                        title: 'Gagal',
                        cek: true,
                        description: controller.uBook.message,
                        onTap: () {},
                      );
                      return;
                    }
                  } else {}
                });
              },
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
              ),
              child: (controller.isLoading1.value == true)
                  ? const Text(
                      'Loading...',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'inter'),
                    )
                  : const Text(
                      'Simpan',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontFamily: 'inter'),
                    ),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        leadingWidth: 76,
        title: Text(
          'Update Book',
          style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            margin: EdgeInsets.only(left: 24, right: 8, top: 8, bottom: 8),
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 4),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              border:
                  Border.all(color: AppColor.grey.withOpacity(0.2), width: 1),
            ),
            child: Icon(
              Icons.arrow_back_ios,
              size: 16,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 24),
        children: [
          SizedBox(height: 32),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.titleC,
            maxLines: null,
            label: 'Title*',
            hint: 'Masukkan title',
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            maxLines: null,
            controller: controller.isbnC,
            label: 'ISBN*',
            hint: 'Masukkan ISBN',
            cek: false,
            cekArray: false,
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          CustomInput(
            maxLines: null,
            controller: controller.priceC,
            label: 'Harga*',
            hint: 'Masukkan Harga',
            cek: false,
            cekArray: false,
            margin: EdgeInsets.only(bottom: 16, top: 16),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 4),
            child: Text(
              'Kategori*',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                fontFamily: 'inter',
              ),
            ),
          ),
          Obx(
            () => (controller.isLoading.value)
                ? Center(child: CircularProgressIndicator())
                : (controller.category.value == null)
                    ? Center(child: CircularProgressIndicator())
                    : SizedBox(
                        height: 48,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            buttonPadding: EdgeInsets.symmetric(horizontal: 16),
                            buttonDecoration: BoxDecoration(
                              border:
                                  Border.all(color: AppColor.grey, width: 1),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            hint: Text(
                              'Select Item',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context).hintColor),
                            ),
                            items: controller.category.value
                                .map<DropdownMenuItem<String>>(
                                    (item) => new DropdownMenuItem<String>(
                                          value: item.id.toString(),
                                          child: Text(item.title),
                                        ))
                                .toList(),
                            value: controller.selectedValue.value,
                            onChanged: (value) {
                              controller.selectedValue.value = value as String;
                            },
                            buttonHeight: 40,
                            buttonWidth: 140,
                            itemHeight: 40,
                          ),
                        ),
                      ),
          ),
          SizedBox(
            height: 20,
          ),
          CustomInput(
            cek: true,
            cekArray: false,
            controller: controller.dateC,
            maxLines: 1,
            label: 'Tanggal Publish*',
            hint: 'dd/mm/yy',
            margin: EdgeInsets.only(bottom: 16),
          ),
          CustomInput(
            cek: false,
            cekArray: false,
            controller: controller.descC,
            maxLines: null,
            label: 'Deskripsi*',
            hint: 'Masukkan deskripsi',
          )
        ],
      ),
    );
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Choose option",
              style: TextStyle(color: Colors.blue),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {},
                    title: Text("Gallery"),
                    leading: Icon(
                      Icons.account_box,
                      color: Colors.blue,
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: Colors.blue,
                  ),
                  ListTile(
                    onTap: () {},
                    title: Text("Camera"),
                    leading: Icon(
                      Icons.camera,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
