import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:test/app/modules/DetailBook/controllers/detail_book_controller.dart';
import 'package:test/app/modules/Home/controllers/home_controller.dart';
import 'package:test/app/routes/app_pages.dart';
import 'package:test/app/widgets/widget.dart';

class HomeView extends GetView<HomeController> {
  HomeView({Key? key}) : super(key: key);
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Home"),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        controller: scrollController,
        child: Container(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Container(
              //   padding: EdgeInsets.only(left: 8, right: 8),
              //   child: TextField(
              //     decoration: InputDecoration(
              //       hintText: 'Search...',
              //       suffixIcon: IconButton(
              //         icon: Icon(Icons.clear),
              //         onPressed: () => (),
              //       ),
              //       // Add a search icon or button to the search bar
              //       prefixIcon: IconButton(
              //         icon: Icon(Icons.search),
              //         onPressed: () {},
              //       ),
              //       border: OutlineInputBorder(
              //         borderRadius: BorderRadius.circular(8.0),
              //       ),
              //     ),
              //   ),
              // ),
              SizedBox(
                width: 500,
                child: Obx(() {
                  if (!controller.isAddLoading.value) {}
                  if (controller.isLoading.value) {
                    return Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return const Center(
                              child: CircularProgressIndicator());
                        },
                        itemCount: controller.isLoading.value
                            ? (controller.bookList.value != null)
                                ? controller.bookList.value.length + 10
                                : 10
                            : (controller.bookList.value != null)
                                ? controller.bookList.value.length + 10
                                : 10,
                      ),
                    );
                  } else if (controller.bookList.value.length > 0) {
                    return ListView.builder(
                      itemCount: controller.bookList.value.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 8),
                      itemBuilder: (context, index) {
                        return InkWell(
                            onTap: () {
                              List temp1 = [
                                '0',
                                controller.bookList.value[index]
                              ];
                              Get.toNamed(Routes.DETAILBOOK,
                                  arguments: json.encode(temp1));
                            },
                            child: CardBook(
                              onTap: () {
                                print("run");
                              },
                              onTapDelete: () {
                                print("run delete");
                              },
                              book: controller.bookList.value[index],
                            ));
                      },
                    );
                  } else {
                    // ignore: curly_braces_in_flow_control_structures, avoid_unnecessary_containers
                    return Container(child: Text("Terjadi Kesalahan!"));
                  }
                }),
              ),
              Obx(() {
                if (controller.isAddLoading.value) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  return MaterialButton(
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    onPressed: () {
                      controller
                          .addBook(controller.bookList.value.length.toString());
                    },
                    child: Text(
                      "Load More",
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                }
              })
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(Routes.ADDBOOK);
        },
        tooltip: 'Add new weight entry',
        child: Icon(Icons.add),
      ),
    );
  }
}
