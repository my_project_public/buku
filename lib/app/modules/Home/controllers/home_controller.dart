import 'package:get/get.dart';
import 'package:test/services/services.dart';
import '../../../../models/model.dart';

class HomeController extends GetxController {
  final count = 0.obs;
  ApiReturnValue bookList =
      ApiReturnValue(value: null, message: "", statusCode: null);
  @override
  void onInit() {
    getBook("0");
    super.onInit();
  }

  void increment() => count.value++;

  RxBool isLoading = RxBool(false);
  RxBool isAddLoading = RxBool(false);

  void getBook(String skip) async {
    try {
      isLoading.value = true;
      ApiReturnValue bookList1 =
          await BookServices.getBook(skip) as ApiReturnValue;

      bookList = bookList1;
    } finally {
      isLoading.value = false;
    }
  }

  void addBook(String skip) async {
    try {
      isAddLoading.value = true;
      ApiReturnValue bookList1 =
          await BookServices.getBook(skip) as ApiReturnValue;
      if (bookList1.value.length > 0) {
        List<Book> items = bookList1.value;
        bookList.value.addAll(items);
      }
    } finally {
      isAddLoading.value = false;
    }
  }
}
