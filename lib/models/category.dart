part of 'model.dart';

class Category extends Equatable {
  final String title;
  final int id;

  const Category({
    required this.title,
    required this.id,
  });

  factory Category.fromJson(Map<String, dynamic> data) => Category(
        title: data['title'],
        id: data['id'],
      );

  Category copyWith({
    required String title,
    required int id,
  }) {
    return Category(
      title: title,
      id: id,
    );
  }

  @override
  List<Object> get props => [title, id];
}

List<Category> categoryDummy = [
  const Category(
    title: "Ibadah",
    id: 1,
  ),
  const Category(
    title: "IT",
    id: 2,
  ),
];
