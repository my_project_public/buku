part of 'model.dart';

class Book extends Equatable {
  final String code;
  final String price;
  final int categoryId;
  final String categoryName;
  final String createdAt;
  final String isbn;
  final String cover;
  final String title;
  final String desc;

  const Book(
      {required this.code,
      required this.price,
      required this.categoryName,
      required this.createdAt,
      required this.isbn,
      required this.cover,
      required this.title,
      required this.desc,
      required this.categoryId});

  factory Book.fromJson(Map<String, dynamic> data) => Book(
        code: data['code'],
        price: data['price'],
        createdAt: data['date_publish'] ?? '',
        categoryName: data['category_name'],
        categoryId: data['category_id'],
        isbn: data['isbn'] ?? '',
        cover: data['cover'] ??
            "https://penerbitdeepublish.com/wp-content/uploads/2020/11/Cover-Buku-DIGITAL-MARKETING-MELALUI-APLIKASI-PLAYSTORE_Usman-Chamdani-depan-scaled-1.jpg",
        title: data['title'],
        desc: data['desc'] ?? "Kosong",
      );

  Book copyWith({
    required String code,
    required String price,
    required String categoryName,
    required String createdAt,
    required String isbn,
    required String cover,
    required String title,
    required String desc,
    required int categoryId,
  }) {
    return Book(
        code: code,
        price: price,
        createdAt: createdAt,
        categoryName: categoryName,
        isbn: isbn,
        cover: cover,
        title: title,
        desc: desc,
        categoryId: categoryId);
  }

  Map<String, dynamic> toJson() {
    return {
      "code": code,
      "price": price,
      "createdAt": createdAt,
      "categoryName": categoryName,
      "isbn": isbn,
      "cover": cover,
      "title": title,
      "desc": desc,
      "categoryId": categoryId,
    };
  }

  @override
  List<Object> get props => [
        code,
        price,
        createdAt,
        categoryName,
        isbn,
        cover,
        title,
        desc,
        categoryId
      ];
}

List<Book> bookDummy = [
  const Book(
      categoryId: 1,
      code: "AUI002",
      price: "10000",
      createdAt: "2014-06-26",
      categoryName: "Ibadah",
      isbn: "121121",
      cover:
          "https://penerbitdeepublish.com/wp-content/uploads/2020/11/Cover-Buku-DIGITAL-MARKETING-MELALUI-APLIKASI-PLAYSTORE_Usman-Chamdani-depan-scaled-1.jpg",
      title: "Perjuangan ibadah",
      desc:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged"),
];
