part of 'services.dart';

class BookServices {
  static var client = http.Client();

  static Future<ApiReturnValue?> getBook(String skip) async {
    var response = await client.post(Uri.parse(baseURL + 'book-get'),
        headers: {
          "Content-Type": "application/json",
        },
        body: jsonEncode(<String, String>{
          'skip': skip,
          'take': "4",
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);

      List<Book> value =
          (data['return'] as Iterable).map((e) => Book.fromJson(e)).toList();

      return ApiReturnValue(
          value: value, statusCode: response.statusCode, message: '');
    } else {
      var data = jsonDecode(response.body);
      // print(response.statusCode);
      return ApiReturnValue(
          message: data['message'],
          statusCode: response.statusCode,
          value: null);
    }
  }

  static Future<ApiReturnValue?> storeBook(
    String code,
    String title,
    String isbn,
    String desc,
    String category,
    String date,
    String price,
  ) async {
    var response = await client.post(Uri.parse(baseURL + 'book-store'),
        headers: {
          "Content-Type": "application/json",
        },
        body: jsonEncode(<String, String>{
          'isbn': isbn.toString(),
          'title': title.toString(),
          'desc': desc.toString(),
          'category_id': category.toString(),
          'date': date.toString(),
          'price': price.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          value: null,
          statusCode: response.statusCode,
          message: 'Berhasil disimpan!');
    } else {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          message: data['message'] ?? "Kode Tidak boleh sama",
          statusCode: response.statusCode,
          value: null);
    }
  }

  static Future<ApiReturnValue?> deleteBook(
    String code,
  ) async {
    var response = await client.post(Uri.parse(baseURL + 'book-delete'),
        headers: {
          "Content-Type": "application/json",
        },
        body: jsonEncode(<String, String>{
          'code': code.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          value: null,
          statusCode: response.statusCode,
          message: 'Berhasil disimpan!');
    } else {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          message: data['message'] ?? "Kode Tidak boleh sama",
          statusCode: response.statusCode,
          value: null);
    }
  }

  static Future<ApiReturnValue?> updateBook(
    String code,
    String title,
    String isbn,
    String desc,
    String category,
    String date,
    String price,
  ) async {
    print(code);
    var response = await client.post(Uri.parse(baseURL + 'book-update'),
        headers: {
          "Content-Type": "application/json",
        },
        body: jsonEncode(<String, String>{
          'isbn': isbn.toString(),
          'title': title.toString(),
          'desc': desc.toString(),
          'category_id': category.toString(),
          'date': date.toString(),
          'price': price.toString(),
          'code': code.toString(),
        }));

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          value: null,
          statusCode: response.statusCode,
          message: 'Berhasil diupdate!');
    } else {
      var data = jsonDecode(response.body);
      print(data);
      return ApiReturnValue(
          message: data['message'] ?? "Kode Tidak boleh sama",
          statusCode: response.statusCode,
          value: null);
    }
  }
}
