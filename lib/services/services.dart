import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'package:test/models/model.dart';

part 'book_service.dart';

String baseURL = 'https://kumeta.absensinow.id/api/';
